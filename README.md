# Notebooks for 1D DMI standard problem for the paper: Proposal for a micromagnetic standard problem for materials with Dzyaloshinskii–Moriya interaction

| Description | Badge |
| --- | --- |
| Bulk DMI notebook | [![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fubermag%2F2023-workshop-boundary-conditions/HEAD?labpath=DMI-bulk-standard-problem.ipynb) |
| Interfacial DMI notebook | [![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fubermag%2F2023-workshop-boundary-conditions/HEAD?labpath=DMI-interface-standard-problem.ipynb) |
| Error with changing discretisation notebook | [![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fubermag%2F2023-workshop-boundary-conditions/HEAD?labpath=deviation-from-shooting-method-changing-cells.ipynb) |

## About

This repository provides notebooks for comparing results from OOMMF and Mumax3 for
a 1D chain of cells in x-direction with exchange, DMI, and uniaxial anisotropy. The
micromagnetic simulation results are compared with shooting method results. The
notebooks are based on the paper: Proposal for a micromagnetic standard problem
for materials with Dzyaloshinskii–Moriya interaction available 
[here](https://iopscience.iop.org/article/10.1088/1367-2630/aaea1c/meta).

There are three notebooks in this repository: one for bulk DMI, one for interfacial,
and one for comparing the error with changing number of cells (discretisation) keeping
the actual length of the chain constant.

## Binder

Jupyter notebooks hosted in this repository can be executed and modified in the
cloud via Binder. This does not require you to have anything installed and no
files will be created on your machine. To access Binder, click on the Binder
badges in the table above. For faster execution we recommend a local installation.
